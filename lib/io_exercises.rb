# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def give_feedback(guess,random_number)
  puts guess
  puts "too low" if guess < random_number
  puts "too high" if guess > random_number
end

def guessing_game
  random_number = 1 + rand(100)
  guess = 0
  number_of_guesses = 0
  puts "guess a number"
  while random_number != guess
    guess = gets.to_i
    number_of_guesses += 1
    give_feedback(guess,random_number)
  end
  puts "You took #{number_of_guesses} tries to guess the right number."
end
